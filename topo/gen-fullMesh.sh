#!/bin/bash

nHost=50

echo "# Name of the Graph:"
echo "Complete Graph (Nodes = 50)"
echo "# Number of Nodes:"
echo "$nHost"
echo "# Number of Edges:"
echo "$((nHost*(nHost-1)))"
echo ""


for ((i=0; i<nHost; i++)); do
	line="$i:"
	count=2
	
	for ((j=0; j<nHost; j++)); do
		if [ $j -ne $i ]; then
			line=$line"$j"
			if [ $count -lt $nHost ]; then
				line=$line";"
			fi
			count=$((count+1))
		fi
	done
	echo "$line"
done
