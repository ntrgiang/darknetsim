//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//



#ifndef DARKNET_STATISTIC_H_
#define DARKNET_STATISTIC_H_

#include "omnetpp.h"

class DnStatistic : public cSimpleModule
{
public:
   DnStatistic();
   virtual ~DnStatistic();

   virtual int numInitStages() const  {return 4;}
   virtual void initialize(int stage);

   virtual void handleMessage(cMessage *msg);
   virtual void handleTimerMessage(cMessage *msg);
   virtual void finish();

   // -- Interface
public:
   void incrementSentMessage(void);
   void incrementNumExpiredTTL(void);
   void incrementReceivedMessage(void);
   void incrementLostMessage(void);
   void incrementNumGenResponse(void);

   // -- Debug
   void recordStartupInstance(void);
   void recordOnDuration(double);
   void recordOffDuration(double);
   void recordActualOnDuration(double);

   void incrementNumOnline(void);
   void decrementNumOnline(void);


protected:
   bool m_debug;

   long m_numSentMessages;
   long m_numExpiredTTL;
   long m_numReceivedMessages;
   long m_numLostMessages;
   long m_numOnlineHosts;

   simsignal_t sig_numSentMsg;
   simsignal_t sig_numExpiredTTL;
   simsignal_t sig_numRecvMsg;
   simsignal_t sig_numLostMsg;

   simsignal_t sig_reachRatio;
   simsignal_t sig_failRatio;
   simsignal_t sig_numGenResponse;

   // -- Debug
   simsignal_t sig_startupInstance;
   simsignal_t sig_onDuration;
   simsignal_t sig_offDuration;
   simsignal_t sig_actualOnDuration;
   simsignal_t sig_numOnline;

};

#endif /* DARKNET_STATISTIC_H_ */
