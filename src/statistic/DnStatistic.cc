//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//



#include "DnStatistic.h"

#include <iomanip> // setprecision

using namespace std;

Define_Module(DnStatistic);

#ifndef debugOUT
#define debugOUT (!m_debug) ? std::cout : std::cout << "@" << setprecision(12) << simTime().dbl() << " @DnStatistic "
#endif


DnStatistic::DnStatistic() {
   // TODO Auto-generated constructor stub
}

DnStatistic::~DnStatistic() {
   // TODO Auto-generated destructor stub
}

void DnStatistic::finish()
{
   emit(sig_numLostMsg, m_numLostMessages);
   emit(sig_numRecvMsg, m_numReceivedMessages);

   double successRatio = (m_numSentMessages) ?
            (double)m_numReceivedMessages/m_numSentMessages
          : 0.0;
   emit(sig_reachRatio, successRatio);

   double failRatio = (m_numSentMessages) ?
            (double)m_numLostMessages/m_numSentMessages
          : 1.0;
   emit(sig_failRatio, failRatio);
}

void DnStatistic::incrementSentMessage()
{
   if (simTime().dbl() < simulation.getWarmupPeriod().dbl())
      return;

   ++m_numSentMessages;
   emit(sig_numSentMsg, m_numSentMessages);
}

void DnStatistic::incrementReceivedMessage()
{
   if (simTime().dbl() < simulation.getWarmupPeriod().dbl())
      return;

   ++m_numReceivedMessages;
   emit(sig_numRecvMsg, m_numReceivedMessages);

   double successRatio = (m_numSentMessages) ?
            (double)m_numReceivedMessages/m_numSentMessages
          : 0.0;
   emit(sig_reachRatio, successRatio);
}

void DnStatistic::incrementLostMessage()
{
   if (simTime().dbl() < simulation.getWarmupPeriod().dbl())
      return;

   ++m_numLostMessages;
   emit(sig_numLostMsg, m_numLostMessages);
}

void DnStatistic::incrementNumExpiredTTL()
{
   if (simTime().dbl() < simulation.getWarmupPeriod().dbl())
      return;

   ++m_numExpiredTTL;
   emit(sig_numExpiredTTL, m_numExpiredTTL);
}


void DnStatistic::recordStartupInstance()
{
   emit(sig_startupInstance, simTime().dbl());
}

void DnStatistic::recordOnDuration(double duration)
{
   emit(sig_onDuration, duration);
}

void DnStatistic::recordOffDuration(double duration)
{
   emit(sig_offDuration, duration);
}

void DnStatistic::recordActualOnDuration(double duration)
{
   emit(sig_actualOnDuration, duration);
}

void DnStatistic::incrementNumOnline()
{
   ++m_numOnlineHosts;
   emit(sig_numOnline, m_numOnlineHosts);
}

void DnStatistic::decrementNumOnline()
{
   --m_numOnlineHosts;
   emit(sig_numOnline, m_numOnlineHosts);
}

void DnStatistic::initialize(int stage)
{
   if (stage == 0)
   {
      m_debug = par("debug");

      m_numSentMessages = 0L;
      m_numExpiredTTL = 0L;
      m_numReceivedMessages = 0L;
      m_numLostMessages = 0L;
      m_numOnlineHosts = 0L;

      sig_numSentMsg = registerSignal("Signal_NumSentMessages");
      sig_numExpiredTTL = registerSignal("Signal_expiredTTL");
      sig_numRecvMsg = registerSignal("Signal_NumReceivedMessages");
      sig_numLostMsg = registerSignal("Signal_NumLostMessages");

      sig_reachRatio = registerSignal("Signal_reachRatio");
      sig_failRatio  = registerSignal("Signal_failRatio");

      // -- Debug
      sig_startupInstance  = registerSignal("Signal_startupInstance");
      sig_onDuration       = registerSignal("Signal_onDuration");
      sig_offDuration      = registerSignal("Signal_offDuration");
      sig_actualOnDuration = registerSignal("Signal_actualOnDuration");
      sig_numOnline        = registerSignal("Signal_numOnline");
   }

   if (stage != 3)
      return;
}

void DnStatistic::handleMessage(cMessage *msg)
{
   if (msg->isSelfMessage())
   {
      handleTimerMessage(msg);
   }
   else
   {
      throw cException("ActivePeerTable doesn't process messages!");
   }
}

void DnStatistic::handleTimerMessage(cMessage *msg)
{
   debugOUT << "--handleTimerMessage--" << endl;
}


