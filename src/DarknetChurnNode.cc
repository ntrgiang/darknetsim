//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "DarknetChurnNode.h"
#include "RandomDistributionFactory.h"
#include "PingMessage_m.h"
#include <csimulation.h>
#include <iomanip>

using namespace std;

//#define TKENV_

#ifdef TKENV_
#define debugOUT EV
#else
#define debugOUT (!m_debug) ? std::cout : std::cout << "@" << setprecision(12) << simTime().dbl() << " @" << m_nodeID << "::DarknetChurnNode: "
#endif

Define_Module(DarknetChurnNode)
;

void DarknetChurnNode::initialize(int stage) {

   DarknetOfflineDetectionNode::initialize(stage);

   if (stage == 0) {
      sigChurnOnOff = registerSignal("sigChurnOnOff");
      sigChurnOff = registerSignal("sigChurnOff");
      sigChurnOn = registerSignal("sigChurnOn");

      startState = par("startState").boolValue();
      m_usePings = par("usePings").boolValue();
      m_pingInterval = par("pingInterval");
      debugOUT << "init::startState = " << startState << endl;
      debugOUT << "usePings = " << m_usePings << endl;
      debugOUT << "m_pingInterval = " << m_pingInterval << endl;

      churnController =
            dynamic_cast<ChurnController *>(simulation.getModuleByPath("churnController"));

      if (churnController == NULL) {
         error(
                  "Could not find ChurnController. Is there one in the network (with name 'churnController')?");
      }

      // Setup online time distribution
      std::string onDist = par("onTimeDistribution").stringValue();
      onTimeDistribution = RandomDistributionFactory::getDistribution(onDist,
                                                                      this, "onTime");
      if (onTimeDistribution == NULL) {
         error(("Unknown random distribution: " + onDist).c_str());
      }

      // Setup offline time distribution
      std::string offDist = par("offTimeDistribution").stringValue();
      offTimeDistribution = RandomDistributionFactory::getDistribution(
               offDist, this, "offTime");
      if (offTimeDistribution == NULL) {
         error(("Unknown random distribution: " + offDist).c_str());
      }

      /* Setup churn controller, i.e. schedule churn events and set online
         * state if needed */
      churnController->doStartup(this);
   }
}

void DarknetChurnNode::handleSelfMessage(cMessage* msg) {
   PingMessage* pmsg = dynamic_cast<PingMessage*>(msg);
   if (m_usePings and (pmsg != NULL)) {
      DarknetMessage* ping = new DarknetMessage("DM_PING");
      ping->setType(DM_PING);
      ping->setDestNodeID(pmsg->getPeerId());
      sendDirectMessage(ping);
      // No re-scheduling here: If no ACK -> remove peer; on ACK arrival it is
      //  re-scheduled
   } else {
      DarknetOfflineDetectionNode::handleSelfMessage(msg);
   }
}
void DarknetChurnNode::handleDarknetMessage(DarknetMessage *msg,
                                            DarknetPeer *sender)
{
   debugOUT << "___handleDarknetMessage___" << endl;

   debugOUT << "received msg of type " << msg->getType() << endl;

   if (m_usePings)
   {
      debugOUT << "scheduled to PING again" << endl;

      // Reset ping timer
      std::map<std::string, PingMessage*>::iterator pmIt = pingMessages.find(
               sender->nodeID);
      if (pmIt != pingMessages.end()) {
         PingMessage* pmsg = pmIt->second;
         cancelEvent(pmsg);
         scheduleAt(calcNextPingTime(), pmsg);
      }
   }

   DarknetOfflineDetectionNode::handleDarknetMessage(msg, sender);
}

void DarknetChurnNode::handleIncomingMessage(DarknetMessage *msg,
                                             DarknetPeer *sender)
{
   debugOUT << "___handleIncomingMessage___" << endl;
   debugOUT << "\t message of type: " << msg->getType() << endl;

   if (msg->getType() == DM_PING) {
      // We don't need this anymore, RCVACK is already sent
      delete msg;
      msg = NULL;
   }
   else
   {
      debugOUT << "let the DnOfflineDetectionNode to handle the message" << endl;
      DarknetOfflineDetectionNode::handleIncomingMessage(msg, sender);
   }
}

void DarknetChurnNode::handleUDPMessage(cMessage* msg) {
   if (isOnline) {
      DarknetOfflineDetectionNode::handleUDPMessage(msg);
   } else {
      // Received UDP packet while offline: Discard
      delete msg;
      msg = NULL;
   }
}

void DarknetChurnNode::sendToUDP(DarknetMessage *msg, int srcPort,
                                 const IPvXAddress& destAddr, int destPort) {
   if (isOnline) {
      DarknetOfflineDetectionNode::sendToUDP(msg, srcPort, destAddr,
                                             destPort);
   } else {
      error("Tried to send UDP packet while offline.");
   }
}

simtime_t DarknetChurnNode::calcNextPingTime() {
   return simTime() + m_pingInterval + uniform(-1, 1, 2);
}

void DarknetChurnNode::addActivePeer(std::string nodeId)
{
   debugOUT << "___addActivePeer___" << endl;

   DarknetOfflineDetectionNode::addActivePeer(nodeId);
   if (m_usePings) {
      PingMessage* pmsg = new PingMessage(("ping " + nodeId).c_str());
      pmsg->setPeerId(nodeId.c_str());
      pingMessages.insert(std::make_pair(nodeId, pmsg));
      scheduleAt(calcNextPingTime(), pmsg);
   }
}

void DarknetChurnNode::removeInactivePeer(std::string peerId) {
   if (m_usePings) {
      std::map<std::string, PingMessage*>::iterator pmIt = pingMessages.find(
               peerId);
      if (pmIt != pingMessages.end()) {
         cancelAndDelete(pmIt->second);
         pingMessages.erase(peerId);
      }
   }
   DarknetOfflineDetectionNode::removeInactivePeer(peerId);
}

void DarknetChurnNode::churnGoOnline() {
   Enter_Method_Silent
         (); // possible context change from churnController
   debugOUT << "___churnGoOnline___" << endl;
   goOnline();
}

void DarknetChurnNode::churnGoOnline(double duration)
{
   debugOUT << "___churnGoOnline2___" << endl;

   m_onlineDuration = duration;
   debugOUT << "host will be online for the next " << m_onlineDuration << " [s]" << endl;
   churnGoOnline();
}

void DarknetChurnNode::goOnline() {
   debugOUT << "___goOnline___" << endl;

   cDisplayString& dispStr = getParentModule()->getDisplayString();
   dispStr.updateWith("i=device/pc2,green");
   emit(sigChurnOn, simTime() - lastSwitch);
   emit(sigChurnOnOff, 1);
   lastSwitch = simTime();
   isOnline = true;

   //connectAllFriends();

//   if (m_numWalksLeft > 0)
//      scheduleSendingRequest();
}

void DarknetChurnNode::markAsOffline() {
   cDisplayString& dispStr = getParentModule()->getDisplayString();
   dispStr.updateWith("i=device/pc2,red");
}

void DarknetChurnNode::churnGoOffline()
{
   Enter_Method_Silent
         (); // possible context change from churnController
   debugOUT << "___churnGoOffline___" << endl;
   goOffline();
}

void DarknetChurnNode::goOffline()
{
   debugOUT << "___goOffline___" << endl;

   DarknetOfflineDetectionNode::crashApp(NULL);

   if (m_usePings)
   {
      std::map<std::string, PingMessage*>::iterator pmIt;
      for (pmIt = pingMessages.begin(); pmIt != pingMessages.end(); pmIt++) {
         cancelAndDelete(pmIt->second);
      }
      pingMessages.clear();
   }

   markAsOffline();
   emit(sigChurnOff, simTime() - lastSwitch);
   emit(sigChurnOnOff, 0);
   lastSwitch = simTime();
   isOnline = false;
}

bool DarknetChurnNode::startApp(IDoneCallback *doneCallback)
{
   debugOUT << "___startApp___" << endl;
   debugOUT << "startState = " << startState << endl;

   if (startState) {
      goOnline();
   } else {
      markAsOffline();
   }
   DarknetOfflineDetectionNode::startApp(doneCallback);
   return true;
}

bool DarknetChurnNode::stopApp(IDoneCallback *doneCallback)
{
   debugOUT << "___stopApp___" << endl;

   return crashApp(doneCallback);
}

bool DarknetChurnNode::crashApp(IDoneCallback *doneCallback)
{
   debugOUT << "___crashApp___" << endl;
   //DarknetOfflineDetectionNode::crashApp(doneCallback);
   goOffline();

   return true;
}

/**
 * Chooses random next hop.
 */
std::vector<DarknetPeer*> DarknetChurnNode::findNextHop(DarknetMessage* msg) {
   DarknetPeer* hop = NULL;
   if (numConnected > 0L) {
      int nextHopIndex = intuniform(0, numConnected - 1, 2);
      int i = 0;
      for (std::map<std::string, DarknetPeer*>::iterator it = m_friendsByID.begin();
           it != m_friendsByID.end(); it++) {
         if (it->second->connected) {
            if (i == nextHopIndex) {
               hop = it->second;
               break;
            }
            i++;
         }
      }
   }

   std::vector<DarknetPeer*> list;
   if (hop != NULL) {
      list.push_back(hop);
   }
   return list;
}
