#ifndef APPCOMMON_H
#define APPCOMMON_H

class PingTimer: public cMessage {
public:
    PingTimer(std::string name) :
            cMessage(name.c_str()) {
    }
};

typedef struct
{
   // -- fields in the message itself
   std::string               destNodeID;
   std::string               srcNodeID;
   DarknetMessageType   type;
   int                  TTL;
   long                 requestMessageID; //if this is a Response: the ID of the Message which requested this
   std::vector<std::string>   prevHosts;
   // -- preamble fields for the destination of the message
   IPvXAddress       destAddress;
   int               destPort;
} MessageObject;

typedef std::map<long, MessageObject> PendingMessages;

#endif // APPCOMMON_H
