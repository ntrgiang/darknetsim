//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "DarknetSimpleNode.h"
#include "PingMessage_m.h"
#include "algorithm"
#include <iomanip>
#include <assert.h>

using namespace std;

//#define TKENV_

#ifdef TKENV_
#define debugOUT EV
#else
#define debugOUT (!m_debug) ? std::cout : std::cout << "@" << setprecision(12) << simTime().dbl() << " @" << m_nodeID << "::DarknetSimpleNode: "
#endif


void DarknetSimpleNode::initialize(int stage)
{
   DarknetBaseNode::initialize(stage);

   if (stage != 6) return;

   // -- #ScheduleSending
   //
   m_requestIntervalMean = par("requestIntervalMean");
   m_requestIntervalVariance = par("requestIntervalVariance");
   debugOUT << "m_requestIntervalMean = " << m_requestIntervalMean << endl;
   debugOUT << "m_requestIntervalVariance = " << m_requestIntervalVariance << endl;

//   scheduleSendingRequest();
}

void DarknetSimpleNode::connectPeer(std::string nodeID) {
   DarknetPeer* peer = m_friendsByID.at(nodeID);
   if (not peer->connected) {
      peer->connected = true;
      // TODO: num connected?!
   }
}

// TODO: Missing disconnectPeer or similar?

void DarknetSimpleNode::handleSelfMessage(cMessage *msg)
{
   debugOUT << "___handleSelfMessage___" << endl;

   if (dynamic_cast<PingTimer*>(msg) != NULL)
   {
      debugOUT << "sending PING to: " << msg->getName() << endl;
      sendMessage(makeRequest(msg->getName()));
      delete msg; msg = NULL;
   }
   else if (dynamic_cast<PingMessage*>(msg) != NULL)
   {
      debugOUT << "A PingMessage" << endl;
      delete msg; msg = NULL;
   }
   else
   {
      debugOUT << "Unidentified timer" << endl;
   }
}
