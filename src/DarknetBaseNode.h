//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef DARKNETBASENODE_H_
#define DARKNETBASENODE_H_

#include <omnetpp.h>
#include <AppBase.h>
#include <IPvXAddress.h>
#include <IUDPSocket.h>
#include "darknetmessage.h"
#include "MessageCallback.h"
#include "./Debug.h"

#include "AppCommon.h"
#include "DnStatistic.h"
#include <stack>
#include <set>

typedef std::pair<IPvXAddress, int> UDPAddress;

typedef struct DarknetPeer DarknetPeer;
struct DarknetPeer
{
   std::string nodeID;
   UDPAddress  address;
   bool        connected;
   double      m_random_interval; // -- Random interval to wait before forwarding
   double      m_lastSeen;

   bool operator<(const DarknetPeer& other) const {
      return address < other.address;
   }
   ;
   bool operator==(const DarknetPeer& other) const {
      return nodeID == other.nodeID;
   }
   ;

   DarknetPeer() : m_random_interval(-1.0) {}
};

typedef struct {
   long packetId;
   simtime_t eventTime;
   std::string srcId;
   std::string destId;
} seenPacket;

#define DARKNET_MESSAGE_ISEXTERNAL "darknet_msg_is_external"
#define DARKNET_MESSAGE_EXTERNAL_CALLBACK "darknet_msg_external_callback"

class DarknetBaseNode: public AppBase {
public:
   DarknetBaseNode() :
      socket(NULL) {
   }
   virtual ~DarknetBaseNode() {
      delete socket;

      std::map<std::string, DarknetPeer*>::iterator frIt;
      for (frIt = m_friendsByID.begin(); frIt != m_friendsByID.end(); frIt++) {
         delete frIt->second;
      }
      m_friendsByID.clear();
   }

   std::string getNodeID();

   virtual void handleExternalMessage(cMessage *msg, simtime_t& when,
                                      MessageCallback* callback);

protected:
   bool m_debug;
   DnStatistic* m_gstat;

   double m_requestIntervalMean, m_requestIntervalVariance;

   std::string m_nodeID;
   IUDPSocket* socket;
   int localPort;
   int defaultTTL;
   int m_numRequestSent;
   std::map<long, MessageObject> m_delayedMessages;

   double m_startTime;
   double m_onlineDuration;
   double param_interval_sendCheck;

   /* Friendlist, indexed by ID and IP address */
   std::map<std::string, DarknetPeer*> m_friendsByID;
   std::map<IPvXAddress, DarknetPeer*> m_friendsByAddress;

   /** Number of currently connected peers */
   long numConnected;

   /** Map for forwarded MessageIDs -> source nodeID */
   std::map<long, std::string> m_forwardedIdTable;
   std::map<long, std::vector<std::string> > forwardedIdTable;

   /** List for responses we are waiting for */
   std::set<long> m_pendingResponses;

   // -- For a random interval from Prob. Dist. file
   static bool m_initStatic;
   static std::string m_distFileName;
   static std::vector<std::pair<long, double> > m_cdf;
   static int m_numHost;
   static std::vector<std::vector<double> > m_latency;

   simsignal_t sigSendDM;
   simsignal_t sigUnhandledMSG;
   simsignal_t sigDropTtlExeeded;
   simsignal_t sigRequestRemainingTTL;
   simsignal_t sigResponseRemainingTTL;

   // -- Local stats, for double check
   //
   //long m_count_numReq

   // Things you probably don't have to change
   virtual int numInitStages() const {
      return 7;
   }
   virtual IUDPSocket* getSocket();
   virtual void connectAllFriends();
   virtual void addPeer(std::string m_nodeID, IPvXAddress& destAddr,
                        int destPort);

   virtual DarknetMessage* makeRequest(std::string m_nodeID);
   virtual DarknetMessage* makeRequest(DarknetMessage *msg,
                                       std::string m_nodeID);
   virtual void makeResponse(DarknetMessage *msg, DarknetMessage *request);
   virtual bool sendDirectMessage(DarknetMessage* msg);
   virtual bool sendMessage(DarknetMessage* msg);
   virtual void sendPacket(DarknetMessage* pkg, IPvXAddress& destAddr,
                           int destPort);
   virtual void sendToUDP(DarknetMessage *msg, int srcPort,
                          const IPvXAddress& destAddr, int destPort);
   virtual IPv4Address getLocalIPv4Address();

   virtual void handleMessageWhenUp(cMessage *msg);
   virtual void handleUDPMessage(cMessage* msg);

   // Things you probably want to implement or extend
   virtual void initialize(int stage);
   virtual bool startApp(IDoneCallback *doneCallback);
   virtual bool stopApp(IDoneCallback *doneCallback);
   virtual bool crashApp(IDoneCallback *doneCallback);

   virtual void handleDarknetMessage(DarknetMessage* msg, DarknetPeer *sender);
   virtual void handleIncomingMessage(DarknetMessage* msg,
                                      DarknetPeer *sender);
   virtual void handleRequest(DarknetMessage* msg, DarknetPeer *sender);
   virtual void forwardMessage(DarknetMessage* msg, DarknetPeer *sender);
   virtual void forwardResponse(DarknetMessage* msg);

   // Things you have to implement
   virtual void connectPeer(std::string m_nodeID) = 0;

   virtual void handleSelfMessage(cMessage* msg) = 0;
   virtual std::vector<DarknetPeer*> findNextHop(DarknetMessage* msg) = 0;

   void printFriendList();
   void printConnectedFriends();
   void printConnectedFriendsShort();

   void printPendingResponse();
   void printForwardedIdTable();
   unsigned int findNumConnectedFriends();
   double getRandomInterval(void);
   double getRandomIntervalFromTrace(void);
   double getLatencyForPair(std::string hostA, std::string hostB);
   bool importLatencyFile(void);
   void buildLatencyLookupTable(void);
   double getSimTimeLimit(void);

protected:
   int getNodeIndex();

};

#endif /* DARKNETBASENODE_H_ */
