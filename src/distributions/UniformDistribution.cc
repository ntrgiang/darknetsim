/*
 * UniformDistribution.cc
 */

#include "UniformDistribution.h"

double UniformDistribution::getNext() {
    return uniform(0, b, 1);
}

double UniformDistribution::getMean()
{
   // CAUTIOUS: should be formula, instead of a specific value
   return 0.5;
}
