/*
 * ExponentialDistribution.cc
 */

#include "ExponentialDistribution.h"

double ExponentialDistribution::getNext() {
    return exponential(mean, randomGenerator);
}

double ExponentialDistribution::getMean()
{
   // CAUTIOUS: should be formula, instead of a specific value
   return 244.74;
}
