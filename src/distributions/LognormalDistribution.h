/*
 * LognormalDistribution.h
 */

#ifndef LOGNORMALDISTRIBUTION_H_
#define LOGNORMALDISTRIBUTION_H_

#include "IRandomDistribution.h"


class LognormalDistribution: public IRandomDistribution {
protected:
    double mean, variance;
    double mu, sigma;
public:
    LognormalDistribution(double mean, double variance, int randomGenerator) :
            IRandomDistribution(randomGenerator), mean(mean), variance(variance)
    {
       mu = log((mean*mean)/(sqrt(variance + mean*mean)));
       sigma = sqrt(log(1 + variance/(mean*mean)));
    }

    virtual double getNext();
    virtual double getMean();
};

#endif /* LOGNORMALDISTRIBUTION_H_ */
