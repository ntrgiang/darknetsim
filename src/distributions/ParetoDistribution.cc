/*
 * ParetoDistribution.cc
 */

#include "ParetoDistribution.h"

double ParetoDistribution::getNext() {
    return pareto_shifted(a, b, c, 1);
}

double ParetoDistribution::getMean()
{
   // CAUTIOUS: should be formula, instead of a specific value
   return 2153.704;
}

