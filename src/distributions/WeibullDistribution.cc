/*
 * WeibullDistribution.cc
 */

#include "WeibullDistribution.h"

double WeibullDistribution::getNext() {
    return weibull(a, b, 1);
}

double WeibullDistribution::getMean()
{
   // CAUTIOUS: should be formula, instead of a specific value
   return 405.9052;
}

