/*
 * PoissonDistribution.cc
 */

#include "PoissonDistribution.h"

double PoissonDistribution::getNext() {
   return poisson(lambda, 1);
}

double PoissonDistribution::getMean()
{
   // CAUTIOUS: should be formula, instead of a specific value
   return 0.5;
}

