/*
 * IRandomDistribution.h
 */

#ifndef IRANDOMDISTRIBUTION_H_
#define IRANDOMDISTRIBUTION_H_

#include <distrib.h>
#include <math.h>

/**
 * Random distributions for churn times. ChurnController assumes result is given
 * in minutes.
 */
class IRandomDistribution {
protected:
    const int randomGenerator;
public:
    IRandomDistribution(int randomGenerator) :
            randomGenerator(randomGenerator) {
    }
    virtual ~IRandomDistribution() {
    }
    virtual double getNext() = 0;
    virtual double getMean() = 0;
};

#endif /* IRANDOMDISTRIBUTION_H_ */
