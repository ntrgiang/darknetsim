//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "DarknetOfflineDetectionNode.h"
#include <cstdio>

#include <iomanip>

using namespace std;

//#define TKENV_

#ifdef TKENV_
#define debugOUT EV
#else
#define debugOUT (!m_debug) ? std::cout : std::cout << "@" << setprecision(12) << simTime().dbl() << " @" << m_nodeID <<  "::DnOfflineDNode: "
#endif

void DarknetOfflineDetectionNode::initialize(int stage) {

   DarknetSimpleNode::initialize(stage);
   switch (stage) {
   case 0:
      rcvack_waiting.clear();
      sigDropResendExeeded = registerSignal("sigDropResendExeeded");
      sigRetransmissionAfterTimeout = registerSignal(
               "sigRetransmissionAfterTimeout");
      param_resendCounter = par("resendCounter");
      param_resendTimerMean = par("resendTimerMean");
      param_resendTimerVariance = par("resendTimerVariance");

      debugOUT << "param_resendCounter = " << param_resendCounter << endl;
      debugOUT << "param_resendTimerMean = " << param_resendTimerMean << endl;
      debugOUT << "param_resendTimerVariance = " << param_resendTimerVariance << endl;

      break;
   case 4:
      break;
   }
}

bool DarknetOfflineDetectionNode::startApp(IDoneCallback *doneCallback) {
   return DarknetBaseNode::startApp(doneCallback);
}

bool DarknetOfflineDetectionNode::stopApp(IDoneCallback *doneCallback) {
   return crashApp(doneCallback);
}

void DarknetOfflineDetectionNode::cancelAllRetransmissions() {
   // Cancel all pending timeouts
   std::map<long, std::pair<DarknetMessage*, int> >::iterator it;
   for (it = rcvack_waiting.begin(); it != rcvack_waiting.end(); it++) {
      cancelAndDelete(it->second.first);
   }
   rcvack_waiting.clear();
}

bool DarknetOfflineDetectionNode::crashApp(IDoneCallback *doneCallback)
{
   debugOUT << "___crashApp___" << endl;

   cancelAllRetransmissions();

   return DarknetBaseNode::crashApp(doneCallback);
}

/*
 * initialize connection establishment by sending a DM_CON_SYN
 */
void DarknetOfflineDetectionNode::connectPeer(std::string nodeID)
{
   debugOUT << "___connectPeer___" << endl;

   DarknetMessage *dm = new DarknetMessage("CON_SYN");
   dm->setType(DM_CON_SYN);
   dm->setTTL(defaultTTL);
   dm->setDestNodeID(nodeID.c_str());
   dm->setSrcNodeID(this->m_nodeID.c_str());
   sendDirectMessage(dm);
}

/**
 * @brief DarknetOfflineDetectionNode::handleIncomingMessage
 * @param msg
 * @param sender
 *
 * Three-step handshake:
 *    - A sends DM_CON_SYN
 *    - when receiving DM_CON_SYN, B sends DM_CON_SYNACK back to A
 *    - when A receives DM_CON_SYNACK, it sends DM_CON_ACK to acknowledge the connection
 *
 */
void DarknetOfflineDetectionNode::handleIncomingMessage(DarknetMessage *msg,
                                                        DarknetPeer *sender)
{
   debugOUT << "___handleIncomingMessage___" << endl;
   debugOUT << "received message of type " << msg->getType() << endl;

   switch (msg->getType()) {
   case DM_CON_SYN: {
      debugOUT << "case DM_CON_SYN_" << endl;

      handleConSynMessage(msg);
      delete msg; msg = NULL;
      break;
   }
   case DM_CON_SYNACK:
   {
      debugOUT << "case DM_CON_SYNACK" << endl;
      handleSynAckMessage(msg);

      delete msg; msg = NULL;
      break;
   }
   case DM_CON_ACK:
   {
      debugOUT << "case DM_CON_ACK" << endl;
      handleConAckMessage(msg);

      delete msg; msg = NULL;
      break;
   }
   default:
      DarknetBaseNode::handleIncomingMessage(msg, sender);
      break;
   }
}

void DarknetOfflineDetectionNode::addActivePeer(std::string nodeId)
{
   debugOUT << "___addActivePeer___" << endl;

   DarknetPeer* peer = m_friendsByID.at(nodeId);
   if (not peer->connected) {
      peer->connected = true;
      numConnected++;
      debugOUT << "Connection to " << nodeId << " established" << endl;
   }
}

/*
 * check if message is of type DM_RCVACK, if so stop resendTimer for according message.
 * if not, send DM_RCVACK to sender and pass it to handleIncomingMessage
 */
void DarknetOfflineDetectionNode::handleDarknetMessage(DarknetMessage* msg,
                                                       DarknetPeer *sender)
{
   debugOUT << "___handleDarknetMessage___" << endl;

   if (msg->getType() == DM_RCVACK) {
      debugOUT << "received DM_RCVACK message" << endl;

      handleRcvAck(msg);
   }
   else
   {
      debugOUT << "received a message rather than DM_RCVACK" << endl;

      sendRcvAck(msg);
      DarknetBaseNode::handleDarknetMessage(msg, sender);
   }
}

void DarknetOfflineDetectionNode::handleRcvAck(DarknetMessage* msg) {
   long orig_mID = msg->getRequestMessageID();
   std::map<long, std::pair<DarknetMessage*, int> >::iterator ackIt =
         rcvack_waiting.find(orig_mID);
   if (ackIt != rcvack_waiting.end()) {
      DarknetMessage* msgPendingAck = ackIt->second.first;

#ifdef USE_DEBUG
      DarknetMessageType type = msgPendingAck->getType();
      long msgId = msgPendingAck->getId();
      DEBUG(
               "Received RCVACK for message: " << type << " (id:" << msgId << ")" << endl);
#endif

      cancelAndDelete(msgPendingAck);
      rcvack_waiting.erase(orig_mID);
   }
   delete msg;
   msg = NULL;
}

void DarknetOfflineDetectionNode::sendRcvAck(DarknetMessage* msg)
{
   DEBUG(
            "Send RCVACK for message: " << msg->getType() << " (ID:" << msg->getId() << "/treeID: " << msg->getTreeId() << ")" << endl);
   DarknetMessage* ack = new DarknetMessage("RCVACK");
   ack->setDestNodeID(msg->getSrcNodeID());
   ack->setType(DM_RCVACK);
   ack->setTTL(defaultTTL);
   ack->setRequestMessageID(msg->getId()); // Or getTreeId()???
   sendDirectMessage(ack);
}

void DarknetOfflineDetectionNode::removeInactivePeer(std::string peerId) {
   DarknetPeer* peer = m_friendsByID.at(peerId);
   if (peer->connected) {
      peer->connected = false;
      numConnected--;
   }
}

/*
 * check if msg is a DarknetMessage in the list of sent messages waiting for an RCVACK.
 * if so, check if resendCounter is reached and otherwise resend (and reschedule it)
 */
void DarknetOfflineDetectionNode::handleSelfMessage(cMessage* msg)
{
   debugOUT << "___handleSelfMessage___" << endl;

   DarknetMessage* dm = dynamic_cast<DarknetMessage*>(msg);
   if (dm != NULL and dm->hasPar("origMsgID")
       and rcvack_waiting.count(dm->par("origMsgID").longValue()) == 1)
   {
      long msgID = dm->par("origMsgID").longValue();
      std::pair<DarknetMessage*, int>* waiting = &rcvack_waiting[msgID];

      if (waiting->second < param_resendCounter)
      {
         int destPort = (int) dm->par("destPort").longValue();
         IPvXAddress* destAddr =
               (IPvXAddress*) (dm->par("destAddr").pointerValue());
         DarknetMessage* dup = dm->dup();
         DarknetBaseNode::sendPacket(dup, *destAddr, destPort);
         emit(sigRetransmissionAfterTimeout,
               dm->par("origMsgID").longValue());

         waiting->second++;
         waiting->first->par("origMsgID").setLongValue(dup->getId());
         rcvack_waiting.insert(std::make_pair(dup->getId(), *waiting));
         rcvack_waiting.erase(msgID);
         scheduleAt(simTime() + normal(param_resendTimerMean, param_resendTimerVariance),
                    dm);
      }
      else
      {
         /* too many resends; delete resendTimer and remove peer from the connected list */
         DEBUG(
                  "Stop resendTimer for message: " << msg << " and remove the peer" << endl);
         emit(sigDropResendExeeded, waiting->first->getTTL());
         removeInactivePeer(dm->getDestNodeID());
         rcvack_waiting.erase(msgID);

         delete dm;
         dm = NULL;
      }
   }
}

void DarknetOfflineDetectionNode::sendPacket(DarknetMessage* pkg,
                                             IPvXAddress& destAddr, int destPort)
{
   debugOUT << "___sendPacket___" << endl;

   // No ACKs for ACKs ... therefore no retransmissions. Also no ACKs for
   //   connection establishment
   if (     (pkg->getType() != DM_RCVACK)
       and  (pkg->getType() != DM_CON_SYN)
       and  (pkg->getType() != DM_CON_SYNACK)
       and  (pkg->getType() != DM_CON_ACK)) {
      DarknetMessage* dup = pkg->dup();

      dup->setName(CS(
                      "Retransmission timeout of "
                      << DarknetMessage::typeToString(pkg->getType())
                      << " #" << pkg->getId()));

      dup->addPar("origMsgID");
      dup->par("origMsgID").setLongValue(pkg->getId());
      dup->addPar("destAddr");
      dup->addPar("destPort");
      dup->par("destAddr").setPointerValue(&destAddr);
      dup->par("destPort").setLongValue(destPort);

      DEBUG(
               "Start retransmission timer for message: (id:" << pkg->getId() << ", DestID: " << pkg->getDestNodeID() << ")" << endl);
      rcvack_waiting.insert(
               std::make_pair(pkg->getId(), std::make_pair(dup, (int) 0)));

      // Minimum reschedule time 10ms
      double rescheduleTime = std::max(
               normal(param_resendTimerMean, param_resendTimerVariance), 0.01);

      scheduleAt(simTime() + rescheduleTime, dup);
   }

   DarknetBaseNode::sendPacket(pkg, destAddr, destPort);
}

void DarknetOfflineDetectionNode::handleConSynMessage(DarknetMessage *msg)
{
   debugOUT << "___handleConSynMessage___" << endl;

   std::map<std::string, DarknetPeer*>::iterator frIt = m_friendsByID.find(
            msg->getSrcNodeID());
   if (frIt != m_friendsByID.end())
   {
      debugOUT << "Received CON_SYN from: " << msg->getSrcNodeID() << endl;
      DarknetMessage *ack = new DarknetMessage("CON_SYNACK");
      ack->setType(DM_CON_SYNACK);
      ack->setTTL(defaultTTL);
      ack->setDestNodeID(msg->getSrcNodeID());
      ack->setSrcNodeID(this->m_nodeID.c_str());
      sendDirectMessage(ack);
   }
}

void DarknetOfflineDetectionNode::handleSynAckMessage(DarknetMessage *msg)
{
   debugOUT << "___handleSynAckMessage___" << endl;

   const char* src_nodeID = msg->getSrcNodeID();
   debugOUT << msg->printToString() << endl;

   std::map<std::string, DarknetPeer*>::iterator frIt = m_friendsByID.find(src_nodeID);
   if (frIt != m_friendsByID.end())
   {
      debugOUT << "Received CON_SYNACK from an old friend: " << src_nodeID << endl;

      DarknetMessage *ack = new DarknetMessage("CON_ACK");
      ack->setType(DM_CON_ACK);
      ack->setTTL(defaultTTL);
      ack->setDestNodeID(src_nodeID);
      ack->setSrcNodeID(this->m_nodeID.c_str());

      sendDirectMessage(ack);

      if (not frIt->second->connected) {
         addActivePeer(msg->getSrcNodeID());
      }
      //printFriendList();
      return;
   }
   debugOUT << "received SYN_ACK from unfriend" << endl;
}

void DarknetOfflineDetectionNode::handleConAckMessage(DarknetMessage *msg)
{
   debugOUT << "___handleConAckMessage___" << endl;

   const char* src_nodeID = msg->getSrcNodeID();
   std::map<std::string, DarknetPeer*>::iterator frIt = m_friendsByID.find(
            src_nodeID);

   if (not frIt->second->connected) {
      addActivePeer(msg->getSrcNodeID());
   }
   //printFriendList();
}

void DarknetOfflineDetectionNode::handleConDisMessage(DarknetMessage *msg)
{
   debugOUT << "___handleConDisMessage___" << endl;
   debugOUT << msg->printToString() << endl;

   const char* src_nodeID = msg->getSrcNodeID();
   std::map<std::string, DarknetPeer*>::iterator iter = m_friendsByID.find(src_nodeID);

   if (iter != m_friendsByID.end())
   {
      iter->second->connected = false;
   }

   //printFriendList();
}
