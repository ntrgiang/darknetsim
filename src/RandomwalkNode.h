//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef RANDOMWALKODE_H_
#define RANDOMWALKNODE_H_

#include "DarknetChurnNode.h"
#include <string>

using namespace std;

class RandomwalkNode : public DarknetChurnNode
{
public:
    RandomwalkNode() : DarknetChurnNode() {}
    virtual ~RandomwalkNode() {}

protected:
    virtual void initialize(int stage);
    virtual void handleDarknetMessage(DarknetMessage* msg, DarknetPeer *sender);
    virtual void handleSelfMessage(cMessage *msg);

    // -- Overloading
    //
    virtual std::vector<DarknetPeer*> findNextHop(DarknetMessage* msg);
    virtual void connectPeer(std::string nodeID);

    virtual void goOnline();
    virtual void churnGoOffline();

    void handleConnectTimer();
    void handleRequestTimer();
    void handleHeartBeatTimer();

    void handleDelayedMessage(long timer_id);

    void processConnectMessage(MessageObject &obj);
    void processConnectAckMessage(MessageObject &obj);
    void processDisconnectMessage(MessageObject &obj);
    void processHeartBeatMessage(MessageObject &obj);
    void processRequestMessage(MessageObject &obj);
    void processResponseMessage(MessageObject &obj);

    std::string getRandomHost(MessageObject &obj);

protected:
    double param_hbInterval;
    double param_requestInterval;
    int m_numGoOnline;

    // -- Helper functions
private:
    int getNumConnectedFriend();
    void copyFromMessageToObject(DarknetMessage& msg, MessageObject& obj);
    void copyFromObjectToMessage(DarknetMessage& msg, MessageObject& obj);
};

#endif /* RANDOMWALKNODE_H_ */
