//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <omnetpp.h>
#include <IPvXAddressResolver.h>
#include <RoutingTable.h>
#include "DarknetBaseNode.h"
#include <UDPPacket.h>
#include <UDPControlInfo.h>
#include <cstringtokenizer.h>
#include <algorithm>
#include <fstream>
#include <string>

#include <iomanip>
#include <assert.h>

//#define TKENV_

#ifdef TKENV_
#define debugOUT EV
#else
#define debugOUT (!m_debug) ? std::cout : std::cout << "@" << std::setprecision(12) << simTime().dbl() << " @" << m_nodeID << "::DarknetBaseNode: "
#endif

std::string   DarknetBaseNode::m_distFileName;
bool     DarknetBaseNode::m_initStatic = false;
int      DarknetBaseNode::m_numHost;
std::vector<std::pair<long, double> >  DarknetBaseNode::m_cdf;
std::vector<std::vector<double> >      DarknetBaseNode::m_latency;

std::string DarknetBaseNode::getNodeID() {
   return m_nodeID;
}

bool DarknetBaseNode::startApp(IDoneCallback *doneCallback)
{
   return true;
}

bool DarknetBaseNode::stopApp(IDoneCallback *doneCallback)
{
   debugOUT << "___stopApp___" << endl;

   // Currently, there's no actual difference between crash and stop.
   return crashApp(doneCallback);
}

bool DarknetBaseNode::crashApp(IDoneCallback *doneCallback)
{
   debugOUT << "___crashApp____" << endl;

   std::map<std::string, DarknetPeer*>::iterator cnIt;
   for (cnIt = m_friendsByID.begin(); cnIt != m_friendsByID.end(); cnIt++) {
      cnIt->second->connected = false;
   }
   numConnected = 0L;
   forwardedIdTable.clear();
   m_forwardedIdTable.clear();
   m_pendingResponses.clear();

   return true;
}

void DarknetBaseNode::addPeer(std::string nodeID, IPvXAddress& destAddr,
                              int destPort) {
   debugOUT << "___addPeer___" << endl;

   DarknetPeer* peer = new DarknetPeer;
   UDPAddress* address = new UDPAddress(destAddr, destPort);
   peer->nodeID = nodeID;
   peer->address = *address;
   peer->connected = false;
   peer->m_random_interval = getLatencyForPair(this->m_nodeID, nodeID);
   m_friendsByID.insert(std::make_pair(nodeID, peer));
   m_friendsByAddress.insert(std::make_pair(destAddr, peer));
}

IUDPSocket* DarknetBaseNode::getSocket() {
   return new IUDPSocket();
}

void DarknetBaseNode::initialize(int stage)
{
   switch (stage) {
   case 0:
   {
      m_debug = (hasPar("debug")) ? par("debug").boolValue() : false;

      localPort   = (int)par("localPort").longValue();
      m_nodeID    = par("nodeID").stdstringValue();
      defaultTTL  = (int)par("defaultTTL").longValue();
      debugOUT << "localPort = " << localPort << endl;
      debugOUT << "nodeID = " << m_nodeID << endl;
      debugOUT << "defaultTTL = " << defaultTTL << endl;

      m_numHost = par("numHost");
      debugOUT << "m_numHost = " << m_numHost << endl;

      numConnected = 0L;
      m_numRequestSent = 0;

      if (!m_initStatic)
      {
         bool importSucceeded = importLatencyFile();
         assert(importSucceeded);

         buildLatencyLookupTable();
      } // init static

      sigSendDM = registerSignal("sigSendDM");
      sigUnhandledMSG = registerSignal("sigUnhandledMSG");
      sigDropTtlExeeded = registerSignal("sigDropTtlExeeded");
      sigRequestRemainingTTL = registerSignal("sigRequestRemainingTTL");
      sigResponseRemainingTTL = registerSignal("sigResponseRemainingTTL");

      break;
   } // case 0
   case 3:
   {
      cModule* temp = simulation.getModuleByPath("statistic");
      m_gstat = check_and_cast<DnStatistic *>(temp);
      debugOUT << " module is completed successfully" << endl;

      socket = getSocket();
      socket->setOutputGate(gate("udpOut"));
      socket->bind(localPort);

      std::vector<std::string> v =
            cStringTokenizer(par("friends")).asVector();

      debugOUT << "number of friends: " << v.size() << endl;
      IPvXAddressResolver resolver = IPvXAddressResolver();
      int count = 0;
      for (std::vector<std::string>::iterator iter = v.begin();
           iter != v.end(); iter++)
      {
         debugOUT << "count = " << count++ << " ";
         std::vector<std::string> peer_tuple = cStringTokenizer(
                  (*iter).c_str(), ":").asVector(); //split <destID>:<destPort>
         if (peer_tuple.size() == 3)
         {
            std::string moduleName = peer_tuple[0];
            std::string nodeID = peer_tuple[1];
            std::istringstream convert(peer_tuple[2]);
            int port;
            port = convert >> port ? port : 0; //convert string to int (user 0 on error)
            IPvXAddress ip = resolver.resolve(moduleName.c_str());
            if (nodeID != this->m_nodeID)
            {
               debugOUT << "to add peer " << nodeID << " - " << ip << " - " << port << endl;
               addPeer(nodeID, ip, port);
            } else {
               error("No friend loops allowed! Check network topology.");
            }
         }
         else
         {
            debugOUT <<
                     "Error on parsing peer list; this peer seems malformed: " << (*iter) << endl;
         }
      } // for
      //printFriendList();
      break;
   } // case 3
   } // switch

   AppBase::initialize(stage - 1);
}

void DarknetBaseNode::connectAllFriends()
{
   debugOUT << "___connectAllFriends___" << endl;

   debugOUT << "Number of friends: " << m_friendsByID.size() << endl;
   for (std::map<std::string, DarknetPeer*>::iterator iter =
        m_friendsByID.begin(); iter != m_friendsByID.end(); iter++) {
      connectPeer(iter->second->nodeID);
   }
}

void DarknetBaseNode::sendToUDP(DarknetMessage *msg, int srcPort,
                                const IPvXAddress& destAddr, int destPort) {
   DEBUG("Sending UDP packet: " << getLocalIPv4Address() << " to ");
   DEBUG(destAddr << ":" << destPort << ", content: ");
   DEBUG(msg->toString() << " (" << msg->getByteLength() << " bytes)" << endl);

   socket->sendTo(msg, destAddr, destPort);
}

void DarknetBaseNode::sendPacket(DarknetMessage* dmsg, IPvXAddress& destAddr,
                                 int destPort) {
   emit(sigSendDM, dmsg->getTreeId());
   sendToUDP(dmsg, localPort, destAddr, destPort);
}

/*
 * sends a DarknetMessage directly to its destination.
 * if destination is not in peers list, just drop it
 */
bool DarknetBaseNode::sendDirectMessage(DarknetMessage* msg)
{
   debugOUT << "___sendDirectMessage___" << endl;
   debugOUT << "::TRACE::message " << msg->getRequestMessageID() << " will be sent now" << endl;

   if (m_friendsByID.find(msg->getDestNodeID()) != m_friendsByID.end())
   {
      DarknetPeer *peer = m_friendsByID[msg->getDestNodeID()];

      debugOUT << "Message to be sent:" << endl;
      debugOUT << msg->printToString() << endl;

      DarknetBaseNode::sendPacket(msg, peer->address.first, peer->address.second);
      return true;
   }
   else
   {
      std::cout << "dest not found" << endl;

      //destination node not in peers list -> not possible to send direct message
      debugOUT << "destination node " << msg->getDestNodeID()
               << " not in peers list -> not possible to send direct message: " << msg << endl;
      return false;
   }
}

bool DarknetBaseNode::sendMessage(DarknetMessage* msg)
{
   debugOUT << "___sendMessage___" << endl;

   std::vector<DarknetPeer*> destPeers = findNextHop(msg);
   if (/*destPeers != NULL and*/destPeers.size() > 0)
   {
      //msg->setSrcNodeID(m_nodeID.c_str());
      for (std::vector<DarknetPeer*>::iterator iter = destPeers.begin();
           iter != destPeers.end(); iter++)
      {
         sendPacket(msg->dup(), (*iter)->address.first,
                    (*iter)->address.second);
      }
      delete msg; msg = NULL;
      return true;
   }
   else
   {
      DEBUG("No next hop found for message: " << msg << endl);
      //TODO: implement proper default error handling here
      delete msg;
      return false;
   }
}

/**
 * Gets the first, non-loopback IPv4 address assigned to this host.
 */
IPv4Address DarknetBaseNode::getLocalIPv4Address() {
   RoutingTable* rt = (RoutingTable*) IPvXAddressResolver().routingTableOf(
            this->getParentModule());
   std::vector<IPv4Address> addresses = rt->gatherAddresses();

   for (std::vector<IPv4Address>::iterator it = addresses.begin();
        it != addresses.end(); it++) {
      if (!it->equals(IPv4Address::LOOPBACK_ADDRESS))
         return *it;
   }

   error("No local IPv4 address found!");
   throw; // make compiler happy, exceptions gets thrown in error()
}

void DarknetBaseNode::handleUDPMessage(cMessage *msg)
{
   debugOUT << "___handleUDPMessage___" << endl;

   DarknetMessage* dm = dynamic_cast<DarknetMessage*>(msg);
   if (dm != NULL)
   {
      UDPDataIndication* udi = (UDPDataIndication*) dm->getControlInfo();
      typedef std::map<IPvXAddress, DarknetPeer*>::iterator it_type;
      it_type it = m_friendsByAddress.find(udi->getSrcAddr());
      if (m_friendsByAddress.end() == it)
      {
         debugOUT << "Only accept a friend as a sender for this message --> ignore it" << endl;
         debugOUT << "Could not get sending peer for message: " << msg << endl;
         debugOUT << "  + Source address: " << udi->getSrcAddr().str()
                  << ":" << udi->getSrcPort() << endl;
         debugOUT << "  + Content of address -> peer map: " << endl;

         //printFriendList();
         delete msg; msg = NULL;
         return;
      }
      DarknetPeer* sender = it->second;
      debugOUT << "Received DarknetMessage at " << m_nodeID
               << " from " << sender->nodeID << endl;
      handleDarknetMessage(dm, sender);
   }
   else
   {
      debugOUT << "received an unknown cMessage: " << msg << endl;
      delete msg;
      msg = NULL;
   }
}

void DarknetBaseNode::handleDarknetMessage(DarknetMessage *msg,
                                           DarknetPeer *sender)
{
   debugOUT << "___handleDarknetMessage___" << endl;

   // follow the mail!!!

   if (msg->getType() == DM_OTHER)
   {
      // Some other message, not interesting for this base class

      debugOUT << "DM_OTHER" << endl;

      handleIncomingMessage(msg, sender);
   }
   else if (forwardedIdTable.find(msg->getRequestMessageID())
            != forwardedIdTable.end())
   {
      // The message is for ME,
      // BUT, it is one of the messages that I forwarded
      // AND, it isn't one of the messages that I triggered sending

      debugOUT << "one of the forwarded Msg" << endl;

      // response for an forwarded Message
      forwardResponse(msg);
   }
   else if (std::find(m_pendingResponses.begin(),
                      m_pendingResponses.end(), msg->getRequestMessageID())
            != m_pendingResponses.end())
   {
      debugOUT << "one of the message sent by me" << endl;

      handleIncomingMessage(msg, sender);
   }
   else
   {
      debugOUT << "something else" << endl;
      // message for this Node
      //handleIncomingMessage(msg, sender);
      forwardMessage(msg, sender);
   }
}

void DarknetBaseNode::handleIncomingMessage(DarknetMessage *msg,
                                            DarknetPeer *sender)
{
   debugOUT << "___handleIncomingMessage___" << endl;

   switch (msg->getType()) {
   case DM_REQUEST:
      debugOUT << "received a DM_REQUEST message" << endl;

      emit(sigRequestRemainingTTL, msg->getTTL());
      handleRequest(msg, sender);
      break;
   case DM_RESPONSE:
      emit(sigResponseRemainingTTL, msg->getTTL());
      m_pendingResponses.erase(msg->getRequestMessageID());

      // -- #Stat
      // log the HIT
      //m_gstat->incrementReceivedMessage();

      delete msg;
      msg = NULL;
      break;
   case DM_OTHER:
      // ignore
      delete msg;
      msg = NULL;
      break;
   default:
      emit(sigUnhandledMSG, msg->getId());
      delete msg;
      msg = NULL;
      break;
   }
}

/*
 * if TTL is > 0; insert the messages treeID in forwardedIdTable and forward it (after decrement TTL)
 * drop it if TTL is = 0
 */
void DarknetBaseNode::forwardMessage(DarknetMessage* msg, DarknetPeer *sender)
{
   debugOUT << "___forwardMessage___" << endl;
   debugOUT << msg->printToString();

   int ttl = msg->getTTL();
   if (ttl > 0)
   {
      msg->setTTL(ttl - 1); //TODO: add probability of TTL reduction

      // -- #Loop
      // Record the sender,
      // Add the sender's ID to the sender list
      //
      std::map<long, std::vector<std::string> >::iterator iter = forwardedIdTable.find(msg->getTreeId());
      if (iter == forwardedIdTable.end())
      {
         // -- no such message has been forwarded before
         std::vector<std::string> id_list;
         id_list.push_back(std::string(msg->getSrcNodeID()));
         forwardedIdTable.insert(
                  std::pair<long, std::vector<std::string> >(msg->getTreeId(), id_list));
      }
      else
      {
         iter->second.push_back(std::string(msg->getSrcNodeID()));
      }

      //printForwardedIdTable();
      sendMessage(msg);
   }
   else
   {
      debugOUT << "TTL expires" << endl;
      emit(sigDropTtlExeeded, msg->getTreeId());

      // -- #TTL
      // should not delete message now,
      // but instead, call forwardResponse(msg, sender)
      forwardResponse(msg);
   }
}

/*
 * forward a Response to a previously forwarded Message back its path "up"
 */
void DarknetBaseNode::forwardResponse(DarknetMessage* msg)
{
   debugOUT << "___forwardResponse___" << endl;
   debugOUT << msg->printToString() << endl;
   //printForwardedIdTable();

   // -- #Loop
   // should forward the message to the last sender
   // then, remove the last sender from list
   //
   std::map<long, std::vector<std::string> >::iterator iter = forwardedIdTable.find(msg->getRequestMessageID());
   if (iter == forwardedIdTable.end()) { delete msg; return; }

   assert(iter != forwardedIdTable.end());
   msg->setDestNodeID(iter->second.back().c_str());
   iter->second.pop_back();
   //printForwardedIdTable();

   sendMessage(msg);
}

void DarknetBaseNode::printFriendList()
{
   debugOUT << "___printFrendList___" << endl;

   debugOUT << "There are " << m_friendsByID.size() << " friends:" << endl;
   std::map<std::string, DarknetPeer*>::iterator iter;
   for (iter = m_friendsByID.begin(); iter != m_friendsByID.end(); ++iter)
   {
      debugOUT << "id: " << iter->first << endl
               << "\t - nodeID: " << iter->second->nodeID << endl
               << "\t - address: " << iter->second->address.first << endl
               << "\t - connected: " << iter->second->connected << endl
               << "\t - interval: " << iter->second->m_random_interval << endl
               << "\t - last seen: " << iter->second->m_lastSeen << endl;
   }
}

void DarknetBaseNode::printConnectedFriends()
{
   debugOUT << "___printConnectedFriends___" << endl;

   std::map<std::string, DarknetPeer*>::iterator iter;
   for (iter = m_friendsByID.begin(); iter != m_friendsByID.end(); ++iter)
   {
      if (!iter->second->connected) continue;

      debugOUT << "id: " << iter->first << endl
               << "\t - nodeID: " << iter->second->nodeID << endl
               << "\t - address: " << iter->second->address.first << endl
               << "\t - interval: " << iter->second->m_random_interval << endl
               << "\t - last seen: " << iter->second->m_lastSeen << endl;
   }
}

void DarknetBaseNode::printConnectedFriendsShort()
{
   debugOUT << "___printConnectedFriendsShort___" << endl;

   std::map<std::string, DarknetPeer*>::iterator iter;
   for (iter = m_friendsByID.begin(); iter != m_friendsByID.end(); ++iter)
   {
      if (!iter->second->connected) continue;
      debugOUT << "id: " << iter->first << endl;
   }
}



void DarknetBaseNode::printPendingResponse()
{
   debugOUT << "___printPendingResponse___" << endl;

   debugOUT << "Number of pending responses: " << m_pendingResponses.size() << endl;
   std::set<long>::iterator iter;
   for (iter = m_pendingResponses.begin();
        iter != m_pendingResponses.end(); ++iter)
   {
      debugOUT << "\t " << *iter << endl;
   }

}

void DarknetBaseNode::printForwardedIdTable()
{
   debugOUT << "___printForwardedIdTable___" << endl;

   std::map<long, std::vector<std::string> >::iterator iter;
   for (iter = forwardedIdTable.begin(); iter != forwardedIdTable.end(); ++iter)
   {
      debugOUT << "\t Message with treeID " << iter->first << " was sent by: " << endl;
      for (std::vector<std::string>::iterator it = iter->second.begin(); it != iter->second.end(); ++it)
      {
         debugOUT << "\t\t - " << (*it).c_str() << endl;
      }
   }
}

unsigned int DarknetBaseNode::findNumConnectedFriends()
{
   std::map<std::string, DarknetPeer*>::iterator iter;
   unsigned int count = 0;
   for (iter = m_friendsByID.begin(); iter != m_friendsByID.end(); ++iter)
   {
      if (iter->second->connected) ++count;
   }
   return count;
}

void DarknetBaseNode::makeResponse(DarknetMessage *msg,
                                   DarknetMessage *request)
{
   debugOUT << "___makeResponse___" << endl;
   debugOUT << "Request:" << endl;
   debugOUT << request->printToString() << endl;

   msg->setType(DM_RESPONSE);
   msg->setTTL(defaultTTL);
   msg->setRequestMessageID(request->getRequestMessageID());
   msg->setDestNodeID(request->getSrcNodeID());
   msg->setSrcNodeID(request->getDestNodeID());

   debugOUT << "Response:" << endl;
   debugOUT << msg->printToString() << endl;
}

void DarknetBaseNode::handleRequest(DarknetMessage* request,
                                    DarknetPeer *sender)
{
   debugOUT << "___handleRequest2___" << endl;

   DarknetMessage *msg = new DarknetMessage("DM_RESPONSE");
   makeResponse(msg, request);

   debugOUT << "::TRACE:: message " << msg->getRequestMessageID() << " (RES) will be sent back" << endl;
   debugOUT << msg->printToString() << endl;

   delete request; request = NULL;
   sendMessage(msg);
}

DarknetMessage* DarknetBaseNode::makeRequest(DarknetMessage *msg,
                                             std::string nodeID)
{
   debugOUT << "___makeRequest2___" << endl;

   msg->setDestNodeID(nodeID.c_str());
   msg->setSrcNodeID(this->m_nodeID.c_str());
   msg->setType(DM_REQUEST);
   msg->setTTL(defaultTTL);
   debugOUT << "a message DM_REQUEST with treeID " << msg->getTreeId() << " has been set up for dest " << nodeID << endl;
   //printPendingResponse();

   return msg;
}

DarknetMessage* DarknetBaseNode::makeRequest(std::string nodeID)
{
   debugOUT << "___makeRequest___" << endl;

   DarknetMessage *msg = new DarknetMessage("DM_REQUEST");
   return makeRequest(msg, nodeID);
}

void DarknetBaseNode::handleExternalMessage(cMessage *msg, simtime_t& when,
                                            MessageCallback* callback = NULL)
{
   Enter_Method_Silent
         ();

   debugOUT << "___handleExternalMessage___" << endl;

   take(msg);
      msg->addPar(DARKNET_MESSAGE_ISEXTERNAL);
      msg->par(DARKNET_MESSAGE_ISEXTERNAL).setBoolValue(true);
      msg->addPar(DARKNET_MESSAGE_EXTERNAL_CALLBACK);
      msg->par(DARKNET_MESSAGE_EXTERNAL_CALLBACK).setPointerValue(
            (void*) callback);

   // Re-Schedule message for arrival, so it will be marked as an event in this
   //   node, not where it came from
   scheduleAt(when, msg);
}

void DarknetBaseNode::handleMessageWhenUp(cMessage *msg) {
   // When the message was transmitted directly via handleExternalMessage(),
   //   clean up the message to make it look as it arrived normally
   if (msg->hasPar(DARKNET_MESSAGE_ISEXTERNAL)
       and (msg->par(DARKNET_MESSAGE_ISEXTERNAL).boolValue())) {
      msg->par(DARKNET_MESSAGE_ISEXTERNAL).setBoolValue(false);
      // Set arrival data, esp. to reset self message property (== in gate -1)
      msg->setArrival(this, gateBaseId("udpIn"));

      // Inform the sender that message arrived
      MessageCallback* callback = (MessageCallback*) msg->par(
               DARKNET_MESSAGE_EXTERNAL_CALLBACK).pointerValue();
      if (callback != NULL) {
         callback->callForMessage(msg);
      }
      msg->par(DARKNET_MESSAGE_EXTERNAL_CALLBACK).setPointerValue(NULL);
   }

   if (msg->isSelfMessage()) {
      handleSelfMessage(msg);
   } else if (msg->getKind() == UDP_I_DATA) {
      handleUDPMessage(msg);
   } else if (msg->getKind() == UDP_I_ERROR) {
      DEBUG("Ignoring UDP error report" << endl);
      delete msg;
      msg = NULL;
   } else {
      error("Unrecognized message (%s)%s", msg->getClassName(),
            msg->getName());
   }
}


double DarknetBaseNode::getRandomInterval()
{
   return 0.3;
   //    return (dblrand());
   //    return getRandomIntervalFromTrace();
}

double DarknetBaseNode::getRandomIntervalFromTrace()
{
   long size = m_cdf.size();
   assert(size > 0);

   long ret = 0;
   double aRand = genk_dblrand(2);

   if (aRand < m_cdf[0].second)
   {
      ret = m_cdf[0].first;
      return (double)ret/1000;
   }

   for(long i = 1; i < size; ++i)
   {
      if (m_cdf[i-1].second <= aRand && aRand < m_cdf[i].second)
      {
         ret = m_cdf[i].first;
         break;
      }
   }

   return (double)ret/1000;
}

double DarknetBaseNode::getLatencyForPair(std::string hostA, std::string hostB)
{
   debugOUT << "___getLatencyForPair___" << endl;

   int num1 = atoi(hostA.erase(0, 4).c_str());
   int num2 = atoi(hostB.erase(0, 4).c_str());
   debugOUT << "num1 = " << num1 << " -- num2 = " << num2 << endl;

   assert(num1 >= 0);
   assert(num2 >= 0);

   return m_latency[num1][num2];
}

bool DarknetBaseNode::importLatencyFile()
{
   const bool IMPORT_FAIL = false;
   const bool IMPORT_SUCCEEDED = true;

   m_distFileName = par("distFileName").str();
   debugOUT << "m_distFileName = " << m_distFileName << endl;
   double sum = 0.0;

   std::ifstream tracefile;
   tracefile.open(par("distFileName"));
   if (!tracefile.good())
   {
      debugOUT << "file " << m_distFileName << " not found" << endl;
      return IMPORT_FAIL;
   }

   if (!tracefile.is_open())
      return IMPORT_FAIL;

   std::string line;
   while (getline(tracefile, line))
   {
      std::vector<std::string> lineParts = cStringTokenizer(line.c_str(), " ").asVector();
      assert(lineParts.size() == 2);

      long number    = atoi(lineParts[0].c_str());
      double aProb   = atof(lineParts[1].c_str());
      if (aProb > 0.0)
      {
         sum += aProb;
         m_cdf.push_back(std::make_pair<long, double>(number, sum));
      }
   }
   tracefile.close();
   m_initStatic = true;

   // -- Debug
   //for (std::vector<std::pair<long, double> >::iterator iter = m_cdf.begin();
   //     iter != m_cdf.end(); ++iter)
   //{
   //   debugOUT << "\t -- number = " << iter->first << " - prob = " << iter->second << endl;
   //}
   //debugOUT << "___sum___ " << sum << endl;

   return IMPORT_SUCCEEDED;
}

void DarknetBaseNode::buildLatencyLookupTable()
{
   debugOUT << "___buildLatencyLookupTable___" << endl;

   // -- pair-wise latency
   //
   std::vector<double> zeros;
   for (int i = 0; i < m_numHost; ++i)
      zeros.push_back(0.0);

   for (int i = 0; i < m_numHost; ++i)
      m_latency.push_back(zeros);

   for (int i = 0; i < m_numHost; ++i)
      for (int j = i+1; j < m_numHost; ++j)
         m_latency[i][j] = m_latency[j][i] = getRandomInterval();

   for (int i = 0; i < m_numHost; ++i)
      for (int j = 0; j < m_numHost; ++j)
         debugOUT << "latency[" << i << "," << j << "] = " << m_latency[i][j] << endl;
}

double DarknetBaseNode::getSimTimeLimit()
{
   std::string s = ev.getConfig()->getConfigValue("sim-time-limit");
   s = s.erase(s.find('s'));
   double sim_time_limit = atof(s.c_str());

   return sim_time_limit;
}


int DarknetBaseNode::getNodeIndex()
{
   std::string str_index = m_nodeID.substr(4,1);
   debugOUT << "str_index = " << str_index << endl;

   return atoi(str_index.c_str());
}

