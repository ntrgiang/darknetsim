//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

//#include <IPAddressResolver.h>
#include "RandomwalkNode.h"
#include "assert.h"
#include <algorithm>
#include <iomanip>
#include <assert.h>
#include "darknetmessage_m.h"

using namespace std;

//#define TKENV_

#ifdef TKENV_
#define debugOUT EV
#else
#define debugOUT (!m_debug) ? std::cout : std::cout << "@" << setprecision(12) << simTime().dbl() << " @" << m_nodeID << "::RandomwalkNode: "
#endif

Define_Module(RandomwalkNode)

void RandomwalkNode::initialize(int stage)
{
   DarknetChurnNode::initialize(stage);

   if (stage == 0)
   {
      param_hbInterval        = par("hbInterval");
      param_requestInterval   = par("requestInterval");
      debugOUT << "param_hbInterval = "      << param_hbInterval << endl;
      debugOUT << "param_requestInterval = " << param_requestInterval << endl;

      m_numGoOnline = 0;
   }
}

void RandomwalkNode::handleDarknetMessage(DarknetMessage *msg, DarknetPeer *sender)
{
   debugOUT << "___handleDarknetMessage___" << endl;
   debugOUT << msg->printToString() << endl;

   if (!isOnline)
   {
      std::cout << "host " << m_nodeID << " is offline now, message discarded" << endl;
      delete msg; msg = NULL;
      return;
   }

   debugOUT << "::TRACE::message " << msg->getRequestMessageID() << " received" << endl;

   // -- Emulate the delay module
   //
   MessageObject obj;
   copyFromMessageToObject(*msg, obj);
   cMessage* delay_timer = new cMessage("DELAY_RECEIVING_TIMER");
   m_delayedMessages.insert(
            std::pair<long, MessageObject>(delay_timer->getId(), obj));

   std::map<std::string, DarknetPeer*>::iterator it = m_friendsByID.find(
            std::string(msg->getSrcNodeID()));
   assert(it != m_friendsByID.end());
   scheduleAt(simTime() + it->second->m_random_interval, delay_timer);

   delete msg; msg = NULL;
}

std::vector<DarknetPeer*> RandomwalkNode::findNextHop(DarknetMessage* msg)
{
   debugOUT << "___findNextHop___" << endl;

   unsigned int num = findNumConnectedFriends();
   debugOUT << "numConnected = " << num << endl;

   if (num == 0)
   {
      // peer list empty -> raise exception?
      debugOUT << "ERROR: empty peer list!" << endl;
      return std::vector<DarknetPeer*>(0);
   }

   std::map<std::string, DarknetPeer*>::iterator frIt = m_friendsByID.find(msg->getDestNodeID());
   if ((frIt != m_friendsByID.end()) and (frIt->second->connected))
   {
      return std::vector<DarknetPeer*>(1, frIt->second);
   }
   else
   {
      debugOUT << "dest is either not a friend or unconnected friend" << endl;
      int randomPeerPos = intuniform(0, num - 1, 2);
      int pos = 0;
      for (frIt = m_friendsByID.begin(); frIt != m_friendsByID.end(); frIt++) {
         if (frIt->second->connected) {
            if (pos == randomPeerPos) {
               return std::vector<DarknetPeer*>(1, frIt->second);
            }
            pos++;
         }
      }
   }
   return std::vector<DarknetPeer*>(0);
}


void RandomwalkNode::handleSelfMessage(cMessage *timer)
{
   debugOUT << "___handleSelfMessage___" << endl;

   if (!isOnline)
   {
      debugOUT << "Host is still ofline, timer to be ignored" << endl;
      delete timer; timer = NULL;
      return;
   }

   if (strcmp(timer->getName(), "DELAY_RECEIVING_TIMER") == 0)
   {
      handleDelayedMessage(timer->getId());
      delete timer; timer = NULL;
   }
   else if (strcmp(timer->getName(), "TIMER_REQUEST") == 0)
   {
      cMessage* timer_request = new cMessage("TIMER_REQUEST");
      scheduleAt(simTime() + param_requestInterval, timer_request);

      handleRequestTimer();
      delete timer; timer = NULL;
   }
   else if (strcmp(timer->getName(), "TIMER_HEART_BEAT") == 0)
   {
      cMessage* hb_timer = new cMessage("TIMER_HEART_BEAT");
      scheduleAt(simTime() + param_hbInterval, hb_timer);

      handleHeartBeatTimer();

      delete timer; timer = NULL;
   }
   else if (strcmp(timer->getName(), "TIMER_CONNECT") == 0)
   {
      handleConnectTimer();
      delete timer; timer = NULL;
   }
   else
   {
      debugOUT << "other timer: " << timer->getName() << endl;
      DarknetOfflineDetectionNode::handleSelfMessage(timer);
   }
}

void RandomwalkNode::churnGoOffline()
{
   Enter_Method_Silent();
   debugOUT << "___churnGoOffline___" << endl;

   // -- #LEAVE
   DarknetMessage* dis_msg = new DarknetMessage("DISCONNECT");
      dis_msg->setType(DM_CON_DIS);
      dis_msg->setTTL(0);
      dis_msg->setRequestMessageID(dis_msg->getTreeId());
      dis_msg->setSrcNodeID(this->m_nodeID.c_str());

   std::map<std::string, DarknetPeer*>::iterator iter;
   for (iter = m_friendsByID.begin(); iter != m_friendsByID.end(); ++iter)
   {
      if (!iter->second->connected) continue;

      dis_msg->setDestNodeID(iter->first.c_str());
      sendPacket(dis_msg->dup(), iter->second->address.first, iter->second->address.second);
   }
   delete dis_msg; dis_msg = NULL;

   // -- Statistics
   //
   m_gstat->recordActualOnDuration(simTime().dbl() - m_startTime);
   m_gstat->decrementNumOnline();

   DarknetChurnNode::churnGoOffline();
}

void RandomwalkNode::handleConnectTimer()
{
   debugOUT << "___handleConnectTimer___" << endl;

   for (std::map<std::string, DarknetPeer*>::iterator iter =
        m_friendsByID.begin(); iter != m_friendsByID.end(); iter++)
   {
      // -- Send CON_SYN messages to friends as heartbeat
      connectPeer(iter->second->nodeID);
   }
}

void RandomwalkNode::handleRequestTimer()
{
   debugOUT << "___handleRequestTimer___" << endl;

   // -- Get a random host
   //
   int cur_node_index = getNodeIndex();
   std::vector<std::string> temp_host_list;
   for(int i = 0; i < m_numHost; ++i)
   {
      if (i == cur_node_index)
         continue;

      std::stringstream stream; stream << i;
      temp_host_list.push_back(std::string("host") + stream.str());
   }

   std::string aRandomDest = temp_host_list[(int)genk_intrand(2, temp_host_list.size())];
   debugOUT << "aRandomHost = " << aRandomDest << endl;

   // -- Make a REQUEST message
   //
   DarknetMessage* m = new DarknetMessage("DM_REQUEST_TEST");
      m->setDestNodeID(aRandomDest.c_str());
      m->setSrcNodeID(this->m_nodeID.c_str());
      m->setType(DM_REQUEST);
      m->setTTL(defaultTTL);
      //m = makeRequest(aRandomDest);
      m->setRequestMessageID(m->getTreeId());
      m->setPrevHostsArraySize(m->getPrevHostsArraySize()+1);
      m->setPrevHosts(m->getPrevHostsArraySize()-1, this->m_nodeID.c_str());

   debugOUT << "::TRACE::message " << m->getRequestMessageID() << " will be sent" << endl;
   debugOUT << m->printToString() << endl;

   // -- Find a next hop
   //
   MessageObject obj;
   copyFromMessageToObject(*m, obj);
   std::string next_hop = getRandomHost(obj);
   if (next_hop == std::string(""))
   {
      debugOUT << "no next hop" << endl;
      delete m; m = NULL;
      return;
   }

   // -- Send request to the next hop
   //
   std::map<std::string, DarknetPeer*>::iterator iter;
   iter = m_friendsByID.find(next_hop);
   assert(iter != m_friendsByID.end());
   assert(iter->second->connected == true);

   m->setPrevHostsArraySize(m->getPrevHostsArraySize()+1);
   m->setPrevHosts(m->getPrevHostsArraySize()-1, next_hop.c_str());
   DarknetBaseNode::sendPacket(m, iter->second->address.first, iter->second->address.second);

   // -- Record the message
   //
   m_pendingResponses.insert(m->getRequestMessageID());

   // -- Statistics
   //
   m_gstat->incrementSentMessage();

   //printPendingResponse();
}


void RandomwalkNode::connectPeer(string nodeID)
{
   debugOUT << "___connectPeer___" << endl;

   DarknetMessage *dm = new DarknetMessage("CON_SYN");
   dm->setType(DM_CON_SYN);
   dm->setTTL(0);
   dm->setDestNodeID(nodeID.c_str());
   dm->setSrcNodeID(this->m_nodeID.c_str());
   dm->setRequestMessageID(dm->getTreeId());
   sendDirectMessage(dm);
}

void RandomwalkNode::goOnline()
{
   debugOUT << "___goOnline___" << endl;

   DarknetChurnNode::goOnline();

   // - - - - - - Additional operations - - - - - -
   ++m_numGoOnline;

   // -- Schedule sending CON_SYN as heartbeat messages after a random period
   //
   if (m_numGoOnline == 1 and startState == true)
   {
      // -- Meaning that host is already in connected state
      //
      std::map<std::string, DarknetPeer*>::iterator iter;
      for(iter = m_friendsByID.begin(); iter != m_friendsByID.end(); ++iter)
         iter->second->connected = true;
   }
   else
   {
      // -- Meaning that host either rejoins OR starts up from OFF state
      //
      cMessage* timer_connect = new cMessage("TIMER_CONNECT");
      scheduleAt(simTime() + genk_dblrand(2), timer_connect);
      //scheduleAt(simTime() + uniform((double)0.0, (double)5.0, 2), timer_connect);
   }

   cMessage* timer_heartbeat = new cMessage("TIMER_HEART_BEAT");
   //scheduleAt(simTime() + 5.0 + genk_dblrand(2), timer_heartbeat);
   scheduleAt(simTime() + uniform((double)5.0, (double)10.0, 2), timer_heartbeat);

   cMessage* timer_request = new cMessage("TIMER_REQUEST");
   //scheduleAt(simTime() + 10.0 + genk_dblrand(2), timer_request);
   scheduleAt(simTime() +  uniform((double)10.0, (double)15.0, 2), timer_request);

   // -- Debug
   m_startTime = simTime().dbl();
   m_gstat->recordStartupInstance();
   m_gstat->incrementNumOnline();
}

void RandomwalkNode::handleHeartBeatTimer()
{
   debugOUT << "___handleHeartBeatTimer___" << endl;

   std::map<std::string, DarknetPeer*>::iterator iter;
   for (iter = m_friendsByID.begin(); iter != m_friendsByID.end(); ++iter)
   {
      if (!iter->second->connected) continue;

      DarknetMessage *hb_pkt = new DarknetMessage("HEART_BEAT");
      hb_pkt->setType(DM_HB);
      hb_pkt->setTTL(0);
      hb_pkt->setDestNodeID(iter->second->nodeID.c_str());
      hb_pkt->setSrcNodeID(this->m_nodeID.c_str());
      hb_pkt->setRequestMessageID(hb_pkt->getTreeId());
      DarknetBaseNode::sendPacket(hb_pkt, iter->second->address.first, iter->second->address.second);
   }
}


void RandomwalkNode::handleDelayedMessage(long timer_id)
{
   debugOUT << "___handleDelayedMessage___" << endl;

   std::map<long, MessageObject>::iterator iter = m_delayedMessages.find(timer_id);
   if (iter == m_delayedMessages.end())
   {
      debugOUT << "timer ID not found" << endl;
      return;
   }

   int msg_type = iter->second.type;
   switch(msg_type)
   {
   case DM_REQUEST:
   {
      processRequestMessage(iter->second);
      break;
   }
   case DM_RESPONSE:
   {
      processResponseMessage(iter->second);
      break;
   }
   case DM_HB:
   {
      processHeartBeatMessage(iter->second);
      break;
   }
   case DM_CON_SYN:
   {
      processConnectMessage(iter->second);
      break;
   }
   case DM_CON_SYNACK:
   {
      processConnectAckMessage(iter->second);
      break;
   }
   case DM_CON_DIS:
   {
      processDisconnectMessage(iter->second);
      break;
   }
   default:
   {
      debugOUT << "unrelated type of message, will be ignored" << endl;
      break;
   }
   } // switch

   m_delayedMessages.erase(iter);
}

void RandomwalkNode::processConnectMessage(MessageObject &obj)
{
   debugOUT << "___processConnectMessage___" << endl;

   debugOUT << "::TRACE::message " << obj.requestMessageID << " (CON_SYN) will be processed now" << endl;
   assert(obj.destNodeID == this->getNodeID());

   std::map<std::string, DarknetPeer*>::iterator iter = m_friendsByID.find(obj.srcNodeID);
   if (iter == m_friendsByID.end())
   {
      debugOUT << "source ID is not a friend, message should be ignored" << endl;
      return;
   }

   iter->second->connected = true;

   // -- To confirm the friendship
   //
   // -- Send back a CON_SYNACK messages to friends as a confirmation
   DarknetMessage *dm = new DarknetMessage("CON_SYNACK");
   dm->setType(DM_CON_SYNACK);
   dm->setTTL(0);
   dm->setDestNodeID(obj.srcNodeID.c_str());
   dm->setSrcNodeID(this->m_nodeID.c_str());
   dm->setRequestMessageID(dm->getTreeId());
   sendDirectMessage(dm);

   //printConnectedFriends();
}

void RandomwalkNode::processConnectAckMessage(MessageObject &obj)
{
   debugOUT << "___processConnectAckMessage___" << endl;

   debugOUT << "::TRACE::message " << obj.requestMessageID << " (CON_SYN) will be processed now" << endl;
   assert(obj.destNodeID == this->getNodeID());

   std::map<std::string, DarknetPeer*>::iterator iter = m_friendsByID.find(obj.srcNodeID);
   if (iter == m_friendsByID.end())
   {
      debugOUT << "source ID is not a friend, message should be ignored" << endl;
      return;
   }

   iter->second->connected = true;
}

void RandomwalkNode::processDisconnectMessage(MessageObject &obj)
{
   debugOUT << "___processDisconnectMessage___" << endl;
   debugOUT << "::TRACE::message " << obj.requestMessageID << " (CON_DIS) will be processed now" << endl;
   assert(obj.destNodeID == this->getNodeID());

   std::map<std::string, DarknetPeer*>::iterator iter = m_friendsByID.find(obj.srcNodeID.c_str());
   if (iter == m_friendsByID.end())
   {
      debugOUT << "source ID is not a friend, message should be ignored" << endl;
      return;
   }

   iter->second->connected = false;
   //printConnectedFriends();
}

void RandomwalkNode::processHeartBeatMessage(MessageObject &obj)
{
   debugOUT << "___processHeartBeatMessage___" << endl;

   debugOUT << "::TRACE::message " << obj.requestMessageID << " (HB) will be processed now" << endl;
   assert(obj.destNodeID == this->getNodeID());

   std::map<std::string, DarknetPeer*>::iterator iter = m_friendsByID.find(obj.srcNodeID.c_str());
   if (iter == m_friendsByID.end())
   {
      debugOUT << "source ID is not a friend, message should be ignored" << endl;
      return;
   }

   iter->second->connected = true;
   iter->second->m_lastSeen = simTime().dbl();
   //printConnectedFriends();
}

void RandomwalkNode::processRequestMessage(MessageObject &obj)
{
   debugOUT << "___processRequestMessage___" << endl;

   if (obj.TTL == 0)
   {
      debugOUT << "TTL expires, should send Response now" << endl;

      // -- Find the destination for the RESPONSE
      //
      std::map<std::string, DarknetPeer*>::iterator it;
      it = m_friendsByID.find(obj.srcNodeID);
      assert(it != m_friendsByID.end());

      // -- Make RESPONSE message
      //
      DarknetMessage *msg = new DarknetMessage("DM_RESPONSE");
         msg->setType(DM_RESPONSE);
         msg->setSrcNodeID(this->m_nodeID.c_str());
         msg->setRequestMessageID(obj.requestMessageID);
         msg->setDestNodeID(it->first.c_str());

      DarknetBaseNode::sendPacket(msg, it->second->address.first, it->second->address.second);

      // -- Statistics
      //
      m_gstat->incrementNumExpiredTTL();

      return;
   }

   debugOUT << "REQUEST should be forwarded" << endl;

   // -- Find a next hop
   //
   std::string next_hop = getRandomHost(obj);
   if (next_hop == std::string(""))
   {
      debugOUT << "no next hop" << endl;
      m_gstat->incrementLostMessage();
      return;
   }
   std::map<std::string, DarknetPeer*>::iterator iter;
   iter = m_friendsByID.find(next_hop);
   assert(iter != m_friendsByID.end());

   // -- Make a REQUEST message
   //
   DarknetMessage* m = new DarknetMessage("DM_REQUEST");
      copyFromObjectToMessage(*m, obj);
      m->setPrevHostsArraySize(m->getPrevHostsArraySize()+1);
      m->setPrevHosts(m->getPrevHostsArraySize()-1, next_hop.c_str());
      m->setTTL(m->getTTL()-1);

   DarknetBaseNode::sendPacket(m, iter->second->address.first, iter->second->address.second);

   // --  Track the sender of this REQUEST
   //
   m_forwardedIdTable.insert(
            std::pair<long, std::string>(obj.requestMessageID, obj.srcNodeID));

   // -- printForwardedIdTable
//   debugOUT << "___printForwardedIdTable___" << endl;
//   std::map<long, std::string>::iterator it;
//   for (it = m_forwardedIdTable.begin(); it != m_forwardedIdTable.end(); ++it)
//   {
//      debugOUT << "message " << it->first << " (REQ) was sent from " << it->second << endl;
//   }
}

void RandomwalkNode::processResponseMessage(MessageObject &obj)
{
   debugOUT << "___processResponseMessage___" << endl;

   // -- Check if the message can be forwarded back
   //
   std::map<long, std::string>::iterator iter;
   iter = m_forwardedIdTable.find(obj.requestMessageID);
   if (iter != m_forwardedIdTable.end())
   {
      debugOUT << "one of the forwarded request" << endl;

      // -- Make RESPONSE message
      //
      DarknetMessage *msg = new DarknetMessage("DM_RESPONSE");
      copyFromObjectToMessage(*msg, obj);
      msg->setDestNodeID(iter->second.c_str());

      std::map<std::string, DarknetPeer*>::iterator it;
      it = m_friendsByID.find(iter->second);
      // TODO: what if the friend now is offline?

      m_forwardedIdTable.erase(iter);
      if (it->second->connected)
         DarknetBaseNode::sendPacket(msg, it->second->address.first, it->second->address.second);

      delete msg; msg = NULL;
      return;
   }

   debugOUT << "originated host not known" << endl;
   // -- Check if the RESPONSE matches the generated REQUEST
   //
   std::set<long>::iterator it;
   it = m_pendingResponses.find(obj.requestMessageID);
   if (it != m_pendingResponses.end())
   {
      debugOUT << "a returning message, GREAT!!!" << endl;
      m_pendingResponses.erase(it);

      // -- Statistics
      //
      m_gstat->incrementReceivedMessage();

      return;
   }

   std::cout << "message with unidentified source, should be ignored!!!" << endl;
   // -- Statistics
   //
   m_gstat->incrementLostMessage();
}

std::string RandomwalkNode::getRandomHost(MessageObject &obj)
{
   debugOUT << "___getRandomHost___" << endl;

   std::vector<std::string> potentialHosts;
   std::map<std::string, DarknetPeer*>::iterator iter;
   int count_online = 0;
   std::vector<std::string> temp = obj.prevHosts;
   for (iter = m_friendsByID.begin(); iter != m_friendsByID.end(); ++iter)
   {
      if (!iter->second->connected) continue;
      ++count_online;
      std::vector<std::string>::iterator it = std::find(temp.begin(),
                                                        temp.end(),
                                                        iter->first);
      if (it != temp.end())
      {
         temp.erase(it);
         continue;
      }
//      std::vector<std::string>::iterator it = std::find(obj.prevHosts.begin(),
//                                                          obj.prevHosts.end(),
//                                                          iter->first);
//      if (it != obj.prevHosts.end()) continue;

      potentialHosts.push_back(iter->first);
   }

   if (potentialHosts.size() == 0)
   {
      debugOUT << "no other hosts to deliver, should drop the request now" << endl;
      debugOUT << "count_online = " << count_online << endl;
      return std::string("");
   }

   assert(potentialHosts.size() > 0);
   std::string aRandomHost = potentialHosts[genk_intrand(2, potentialHosts.size())];
   debugOUT << "a random next hop: " << aRandomHost << endl;

   return aRandomHost;
}

int RandomwalkNode::getNumConnectedFriend()
{
   int count = 0;
   std::map<std::string, DarknetPeer*>::iterator iter;
   for (iter = m_friendsByID.begin(); iter != m_friendsByID.end(); ++iter)
   {
      if (iter->second->connected) ++count;
   }
   return count;
}


void RandomwalkNode::copyFromMessageToObject(DarknetMessage &msg, MessageObject &obj)
{
   obj.destNodeID = msg.getDestNodeID();
   obj.srcNodeID  = msg.getSrcNodeID();
   obj.type       = msg.getType();
   obj.TTL        = msg.getTTL();
   obj.requestMessageID = msg.getRequestMessageID();

   if (obj.type != DM_REQUEST)
      return;

   unsigned int num_host = msg.getPrevHostsArraySize();
   if (num_host == 0) return;

   for (unsigned int i = 0; i < num_host; ++i)
   {
      obj.prevHosts.push_back(msg.getPrevHosts(i));
   }
}

void RandomwalkNode::copyFromObjectToMessage(DarknetMessage &msg, MessageObject &obj)
{
   unsigned int num_host = obj.prevHosts.size();

   msg.setDestNodeID(obj.destNodeID.c_str());
   msg.setSrcNodeID(obj.srcNodeID.c_str());
   msg.setType(obj.type);
   msg.setTTL(obj.TTL);
   msg.setRequestMessageID(obj.requestMessageID);

   if (obj.type != DM_REQUEST)
      return;

   msg.setPrevHostsArraySize(num_host);
   if (num_host == 0) return;

   for (unsigned int i = 0; i < num_host; ++i)
   {
      msg.setPrevHosts(i, obj.prevHosts[i].c_str());
   }
}
