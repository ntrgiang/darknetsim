#!/bin/sh

config=$1
ini_file=$2
exec_file=../src/darknetsim

# -- Cmdenv
$exec_file= -r 0 -u Cmdenv -c $config -n .:../src:../../inet22/examples:../../inet22/src -l ../../inet22/src/inet $ini_file | tee /tmp/darknetsim.log

# -- TKenv
# $exec_file= -r 0 -c $config -n .:../src:../../inet22/examples:../../inet22/src -l ../../inet22/src/inet $ini_file | tee /tmp/darknetsim.log
