#!/bin/bash

#SBATCH --partition=parallel
#SBATCH --constraint=dual
#SBATCH --ntasks=10
#SBATCH --cpus-per-task=1
#SBATCH --array=0-29:10
#SBATCH --mem-per-cpu=3000
#SBATCH --time=05:00:00

