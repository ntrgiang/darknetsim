#!/bin/bash

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Description: copy all the following files to a separate folder
#   - configuration files (NED & INI) 
#   - result files
# Purpose: for backup and post-processing
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Syntax:
#    <script> <server> <part> <container_folder> <folder_name> <configuration_name> <configuration_file> 

#    $1: <server>             -- either p2pram or fuchs
#    $2: <part>               -- a numeric number
#    $3: <container_folder>   -- full path to containing folder
#    $4: <folder_name>        -- name of the folder that will store data
#    $5: <configuration_name> -- name of configuration (from that to form the result files)
#    $5: <configuration_file> -- name of the INI file

# Example:
#    <script> 
#       p2pram 
#       1 
#       /dos/research/experiments/scheduling-diversification/data/lcn/baseline
#       donet--vs-time--1000n--nPS10--nP6--pO-01-05
#       inet_pull
#       pull.ini
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# -- Inputs
#
server=$1
part=$2

container_folder=$3
folder_name=$4

config_name=$5
ini_file=$6

echo "Copying data from the remote machine ..."

# -- Preparation
#
store_dir=$container_folder/$folder_name

if [ "$server" = "fuchs" ]; then
	remote=$server:/home/darmstadt/nguyeng
else
	remote=$server:/home/giang
fi

remote=$remote/sim/"darknet_"$part/simulations

# -- Preparation
#
mkdir $store_dir

# -- Copy
#
scp $remote/results/$config_name-*.* $store_dir

scp $remote/$ini_file $store_dir

echo " ... done!"

