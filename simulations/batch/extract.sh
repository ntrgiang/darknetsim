#!/bin/sh

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Description: 
#     - calculate mean & confidence intervals, 
#     - then print out the calculated data
#
# Purpose: prepare for plots
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Syntax:
#    <script> <container_folder> <folder_name> <metric_name>

# Example:
#    $1=/dos/research/experiments/scheduling-diversification/data/breath-first-attacker/impact-of-nPS-36runs
#    $2=RF-BW--vs-numAttack_static_700s_500n_400kbps_nP4_pOffset02_bmInt05_schedInt08_prInt10s_pM08_36runs_nPS2
#    $3=damage_ratio:last
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

source config.ini

echo "#var average lower upper ci_percent min max"

container_folder=$1
folder_name=$2
metric=$3

data_folder=$container_folder/$folder_name

for sub in "${xValue[@]}"; do
   grep $metric $data_folder/$sub/*.sca | awk '{print $4}' | awk -v var=$sub '
NR == 1 { max=$1; min=$1; sum=0; }
{ if ($1>max) max=$1; if ($1<min) min=$1; sum+=$1; }
{ ln++; value[ln]=$1; }
END {
   if (NR>1)
   {
      average = sum/NR;
      for(i=1; i<=ln; i++)
      {
         gap = value[ln] - average;
         sum2 = sum2 + gap*gap;
      }
      variance = sqrt(sum2/(ln-1));
      #ci = 1.96 * variance/sqrt(ln);
      ci = 1.645 * variance/sqrt(ln);
      ci_percent = (average>0) ? ci*100 / average : 0;
      lower = average - ci;
      upper = average + ci;
   }
   else
   {
      average=sum;
      lower=upper=average;
      ci_percent=0;
      min=max=average;
   }
   printf "%.2f %.5f %.5f %.5f %.5f %.5f %.5f \n", var, average, lower, upper, ci_percent, min, max
}'
done

