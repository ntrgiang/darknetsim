#!/bin/bash

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Description: a wrapper script which uses the following scripts
#     - move-data.sh
#     - calculate-all-metrics.sh (this in turn, calls extract.sh)
#
# Purpose: reduce the task to provide parameters to individual scripts
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Syntax:
#    <script> <folder_name> <configuration_name> <configuration_file> <repeat>

# Example:
#    $1=RF-BW--vs-numPartner--500n-srate500kbps-upbw2500kbps-5partners (this is the string which describe the single simulation configuration)
#    $2=Donet_oneRouter_overlayAttack_new
#    $3=Donet-overlayAttack (NOTE!!! __without__ .ini)
#    $4=30
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# What needed to do:
#    - modify the range in the loop @ file (this is done once for the running variable):
#         . move-data.sh
#         . extract.sh
#    - Change the container_folder in the three files:
#         . move-data.sh (to know where to move and distribute data from/to))
#         . extract.sh (to know from where to take data for calculation)
#         . calculate-all-metrics.sh (to writing the results from extract.sh
#    - provide all metric_name and their store files in the script calculate-all-metrics.sh (this can be rather stable through one simulation study)

data_folder=$1
configuration_name=$2
configuration_file=$3
repeat=$4

bash ./move-data.sh "$data_folder" "$configuration_name" "$repeat"

bash ./calculate-all-metrics.sh "$data_folder"

