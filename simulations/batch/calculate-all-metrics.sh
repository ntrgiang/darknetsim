#!/bin/bash

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Description: 
#     - calculate all provided metrics
#     - call the extract.sh script to do the job
#
# Purpose: automate the repeated works
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Syntax:
#    <script> <container_folder> <folder_name>

# Example:
#    $1=/dos/research/experiments/scheduling-diversification/data/breath-first-attacker/impact-of-nPS-36runs
#    $2=RF-BW--vs-numAttack_static_700s_500n_400kbps_nP4_pOffset02_bmInt05_schedInt08_prInt10s_pM08_36runs_nPS2
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
source config.ini

echo "Calculating all metrics ..."

container_folder=$1
folder_name=$2

data_folder=$container_folder/$folder_name

#for i in 12 13 14 15 16 17
for (( i=0; i<${#metric[@]}; i++ ))
do
   #echo "Extracting & Calculating for metric ${metric[$i]} ... & folder: $data_folder"
   rm $data_folder/${filename[$i]}
   bash ./extract.sh $container_folder $folder_name "${metric[$i]}" >> "$data_folder/${filename[$i]}"
done

echo " ... done!"
