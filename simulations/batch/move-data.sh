#!/bin/bash

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Description: move data files into subfolders according to a running parameter
#
# Purpose: prepare for post-processing
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Syntax:
#    <script> <container_folder> <folder_name> <configuration_name> <repeat>

# Example:
#    $1=/dos/research/experiments/scheduling-diversification/data/breath-first-attacker/impact-of-nPS-36runs
#    $2=RF-BW--vs-numAttack_static_700s_500n_400kbps_nP4_pOffset02_bmInt05_schedInt08_prInt10s_pM08_36runs_nPS2
#    $2=Donet_oneRouter_overlayAttack_new
#    $3=36
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

source config.ini

echo "Move data and separate them into subfolders ..."

container_folder=$1
folder_name=$2

config=$3
repeat=$4

data_folder=$container_folder/$folder_name

loop=0

for sub in "${xValue[@]}"; do
   sub_dir=$data_folder/$sub
   mkdir $sub_dir
   lower=$((loop*repeat))
   upper=$((loop*repeat+repeat-1))
   for (( i=$lower; i<=$upper; i++ ))
   do
      file=$config-$i.sca
      mv $data_folder/$file $sub_dir
   done
   loop=$((loop+1))
done

echo " ... done!"
