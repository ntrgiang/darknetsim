#!/bin/bash

# Assuming that the file is executed within the run-script/ folder

# $1 = Configuration name
# $2 = INI file which stores the configuration

#config=Donet_oneRouter_underlayAttack 
#ini_file=../Donet-underlayAttack.ini

config=$1
ini_file=$2
exec_file=../src/darknetsim

rm /tmp/darknetsim.log

gdb --args $exec_file -r 0 -u Cmdenv -c $config $ini_file -n ../src:../simulations:../../inet22/src | tee /tmp/darknetsim.log
