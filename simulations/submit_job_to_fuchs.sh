#!/bin/bash

rm *.out
rm ./results/*.*
cp Kad.txt /tmp/

part=4
sim_dir="/home/darmstadt/nguyeng/sim"

exec_file="$sim_dir/darknet_"$part"/src/darknetsim"
config="fullMesh10"
ini_file="FullMesh.ini"

num_run=`$exec_file -x $config $ini_file | grep "Number of runs:" | cut -d' ' -f4`
last_run=$((num_run-1))
num_core=12
mem_per_cpu=4000 #MB

batch_file="mybatch.sh"
rm $batch_file

echo "#!/bin/bash" >> $batch_file
echo "#SBATCH --partition=parallel" >> $batch_file
#echo "#SBATCH --constraint=dual" >> $batch_file
echo "#SBATCH --ntasks=$num_core" >> $batch_file
echo "#SBATCH --cpus-per-task=1" >> $batch_file
#echo "#SBATCH --nodes=1-1" >> $batch_file
echo "#SBATCH --mem-per-cpu=$mem_per_cpu" >> $batch_file
echo "#SBATCH --time=05:00:00" >> $batch_file
echo "#SBATCH --array=0-$last_run:$num_core" >> $batch_file

echo "sim_dir=$sim_dir" >> $batch_file
echo "exec_file=$exec_file" >> $batch_file
echo "config=$config" >> $batch_file
echo "ini_file=$ini_file" >> $batch_file

echo "opp_runall -j$num_core $exec_file -r 0..$last_run -u Cmdenv -c $config $sim_dir/darknet_$part/simulations/$ini_file -n $sim_dir/darknet_$part/simulations:$sim_dir/darknet_$part/src:$sim_dir/inet22/src -l $sim_dir/inet22/src/inet" >> $batch_file

echo "wait" >> $batch_file

sbatch $batch_file
