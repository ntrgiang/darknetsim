#!/bin/sh

config=$1
ini_file=$2
exec_file=../src/darknetsim

rm ./results/*.*
cp Kad.txt /tmp/

num_run=`$exec_file -x $config $ini_file | grep "Number of runs:" | cut -d' ' -f4`
echo "Total number of run: $num_run"
last_run=$((num_run-1))

# -- Cmdenv
opp_runall -j1 $exec_file -r 0 -u Cmdenv -c $config -n .:../src:../../inet22/examples:../../inet22/src -l ../../inet22/src/inet $ini_file | tee /tmp/darknetsim.log

# -- TKenv
# opp_runall -j1 $exec_file -r 0 -c $config -n .:../src:../../inet22/examples:../../inet22/src -l ../../inet22/src/inet $ini_file | tee /tmp/darknetsim.log

# -- Cmdenv -- multi runs
#opp_runall -j2 $exec_file -r 0..$last_run -u Cmdenv -c $config -n .:../src:../../inet22/examples:../../inet22/src -l ../../inet22/src/inet $ini_file | tee /tmp/darknetsim.log

# -- Send email after the completion of the simulation
#
#echo "darknetsim DONE!" | mail -s "Simulation @ p2pram"  nguyen@cs.tu-darmstadt.de

