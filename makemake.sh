#!/bin/sh

rm -rf out
cd ./src

# Make Makefiles
opp_makemake -f --deep -o darknetsim -O out -linet -I../../inet22/src/networklayer/ipv4 -I../../inet22/src/networklayer/common -I../../inet22/src/networklayer/rsvp_te -I../../inet22/src/networklayer/icmpv6 -I../../inet22/src/transport/tcp -I../../inet22/src/base -I../../inet22/src/networklayer/mpls -I../../inet22/src/networklayer/ted -I../../inet22/src/util/headerserializers -I../../inet22/src/networklayer/contract -I../../inet22/src/util -I../../inet22/src/transport/contract -I../../inet22/src/networklayer/ipv6 -I../../inet22/src/transport/sctp -I../../inet22/src/linklayer/mfcore -I../../inet22/src/world -I../../inet22/src/applications -I../../inet22/src/applications/pingapp -I../../inet22/src/linklayer/contract -I../../inet22/src/networklayer/arp -I../../inet22/src/networklayer/ldp -I../../inet22/src/transport/udp -I../../inet22/src/applications/udpapp -I../../inet22/src/status -I../../omnetpp-4.2.2/include/ -I/usr/include/ -I/usr/include/c++/4.6 -I../../boost/include -I../../gmp-5.1.3 -KINET_PROJ=../../inet -L/usr/lib -L../../inet22/src

# Resolve dependencies
make depend

#make MODE=release
